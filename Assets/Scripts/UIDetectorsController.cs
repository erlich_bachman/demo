﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIDetectorsController : MonoBehaviour {

    public GameObject[] detectors;
    public Vector2[] normalPoses;
    private float delta;
    public bool first;
    public GameObject[] slots;


    void Start()
    {
        delta = normalPoses[0].x - normalPoses[1].x;
        foreach (GameObject detector in detectors)
            detector.SetActive(false);
    }

    public void firstToBool(bool state)
    {
        first = state;
    }
    public void setDetector(int detectorIndex)
    {
        if (slots[Convert.ToInt32(!first)] != null)
            slots[Convert.ToInt32(!first)].SetActive(false);
        detectors[detectorIndex].SetActive(true);
        detectors[detectorIndex].GetComponent<RectTransform>().localPosition = normalPoses[0] - Vector2.right * delta * Convert.ToInt32(!first);
        slots[Convert.ToInt32(!first)] = detectors[detectorIndex];
    }



}
