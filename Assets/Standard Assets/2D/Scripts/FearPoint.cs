﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FearPoint : MonoBehaviour {

    public float fearstrength;
    public Text strength;
    public int speed;
    private UnityStandardAssets._2D.PlatformerCharacter2D player;
    private float startPosX;

    // Use this for initialization
    void Start () {
        startPosX = transform.position.x;
        player = GameObject.Find("2DCharacter").GetComponent<UnityStandardAssets._2D.PlatformerCharacter2D>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3( startPosX + Mathf.PingPong(Time.time * speed, 5), transform.position.y, transform.position.z);
    }

    public void forceStrangeMinus()
    {
        if (player.detectors[0].IsActive() && player.detectors[1].IsActive() && player.detectors[0].fillAmount != 0)
        {
            fearstrength -= 1;
            strength.text = fearstrength.ToString();
            if (fearstrength == 0)
            {
                GetComponentInChildren<ParticleSystem>().Stop();
                strength.enabled = false;
                GetComponent<CircleCollider2D>().enabled = false;
                player.percentage = .5f;
                player.sign = -1;
                foreach(Image detector in player.detectors)
                {
                    detector.fillAmount = 0f;
                }
            }
        }
    }


    public void forceStrangePlus()
    {
        fearstrength += 1;
        strength.text = fearstrength.ToString();
    }

}
